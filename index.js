// console.log("Hello");

let trainer = {
  name : "Ash Ketchum",
  
  age: '10',
  pokemon : ["Pikachu","Charizard","Squirtle","Bulbasaur"],
  Friends :  {hoen : ["May", "Max"], kanto:["Brock","Misty"] }
          

}
console.log(trainer);
trainer.talk = function() {
  
 console.log("Result of Dot notation:");
 console.log(trainer.name);
 console.log("Result of square bracket notation:");
 console.log(trainer["pokemon"]);
 console.log("Result of talk method: ");
  console.log(this.pokemon[0] + ' I choose you!');

}
trainer.talk();




function Pokemon(name, level){

  //Properties
  this.name = name;
  this.level = level;
  this.health = level *2 ;
  this.attack = level = level;

  this.tackle = function(target){
      console.log(this.name + " tackled " + target.name);
      
      target.health -= this.attack
      console.log(target.name + " health is now reduceed to "+target.health);

      if(target.health <= 0){
          target.faint();
      }
  }
  this.faint = function(){
      console.log(this.name + " fainted ");
  }
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);

mewtwo.tackle(geodude);

console.log(geodude);
